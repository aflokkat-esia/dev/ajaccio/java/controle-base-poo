public class Main {
    public static void main(String[] args) {
        Developpeur quentin = new Developpeur("Quentin");
        MachineACafe machine = new MachineACafe(50, 20, 20);

        while (quentin.developpe() && quentin.faitUnePause(machine)) {
            quentin.donnerUnCoupDePied(machine);
        };
        quentin.etat();
    }
}