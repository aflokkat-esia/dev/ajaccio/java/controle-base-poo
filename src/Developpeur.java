public class Developpeur {
    private final String nom;
    private int energie = 100;
    private int argent = 10;

    private int energiePerdueEnDeveloppant = 40;

    public Developpeur(String nom) {
        this.nom = nom;
    }

    public int getEnergie() {
        return this.energie;
    }

    public void setEnergie(int energie) {
        if (energie > 100) {
            this.energie = 100;
        } else this.energie = Math.max(energie, 0);
    }

    public int getArgent() {
        return this.argent;
    }

    public void setArgent(int argent) {
        this.argent = Math.max(argent, 0);

        if (this.argent == 0) {
            System.out.println("AAAHHH !!! Plus d'argent !");
            this.setEnergie(this.getEnergie() - 30);
        }
    }

    public int getEnergiePerdueEnDeveloppant() {
        return this.energiePerdueEnDeveloppant;
    }

    public void setEnergiePerdueEnDeveloppant(int energiePerdueEnDeveloppant) {
        this.energiePerdueEnDeveloppant = Math.max(energiePerdueEnDeveloppant, 0);
    }

    public void infos() {
        System.out.println(" -> Développeur " + this.nom + ", " + this.energie + " d'énergie, " + this.argent + " d'argent");
    }

    public boolean developpe() {
        System.out.println("Je développe ! (-" + this.energiePerdueEnDeveloppant + " d'énergie)");
        this.setEnergie(this.getEnergie() - this.energiePerdueEnDeveloppant);
        this.infos();
        return this.energie != 0 && this.energie != 100;
    }

    public void faitUnePause() {
        System.out.println("Je fais une pause !");
        this.setEnergie(this.getEnergie() + 20);
        this.infos();
    }

    public boolean faitUnePause(MachineACafe machine) {
        System.out.println("Je fais une pause avec café !");
        machine.faitUnCafe(this);
        this.infos();
        return this.energie != 0 && this.energie != 100 && this.argent != 0;
    }

    public void donnerUnCoupDePied(MachineACafe machine) {
        System.out.println("Meurs, pourriture communiste !");
        this.setEnergiePerdueEnDeveloppant(this.getEnergiePerdueEnDeveloppant() - 10);
        machine.setProbabiliteBonCafe(machine.getProbabiliteBonCafe() + 0.3f);
    }

    public void etat() {
        if (this.energie == 100) {
            System.out.println("C'est la victoire de l'Homme face à la Machine grâce au Saint Café !");
        } else {
            System.out.println("Steven, you are a FAILURE!!!");
        }
    }
}
