import java.util.Random;

public class MachineACafe {
    int energieRegagnee = 50;
    int energiePerdueQuandMauvaisCafe = 20;
    int energiePerdueQuandCafeCher = 20;
    float probabiliteBonCafe = 0.3f;

    public MachineACafe() {}
    public MachineACafe(int energieRegagnee, int energiePerdueQuandMauvaisCafe, int energiePerdueQuandCafeCher) {
        this.energieRegagnee = energieRegagnee;
        this.energiePerdueQuandMauvaisCafe = energiePerdueQuandMauvaisCafe;
        this.energiePerdueQuandCafeCher = energiePerdueQuandCafeCher;
    }

    public float getProbabiliteBonCafe() {
        return probabiliteBonCafe;
    }

    public void setProbabiliteBonCafe(float probabiliteBonCafe) {
        if (probabiliteBonCafe < 1f && probabiliteBonCafe > 0f) this.probabiliteBonCafe = probabiliteBonCafe;
    }

    public void faitUnCafe(Developpeur dev) {
        Random r = new Random();
        int cout = r.nextInt(2) + 1;
        dev.setArgent(dev.getArgent() - cout);
        if (cout == 2) {
            dev.setEnergie(dev.getEnergie() - this.energiePerdueQuandCafeCher);
            System.out.println("Et un café cher ! (-" + this.energiePerdueQuandCafeCher + " d'énergie)");
        }

        System.out.println("Café en cours...");
        if (Math.random() < probabiliteBonCafe) {
            System.out.println("Et c'est un bon café ! (+" + this.energieRegagnee + " d'énergie)");
            dev.setEnergie(dev.getEnergie() + this.energieRegagnee);
        } else {
            System.out.println("C'café, il a l'gout d'une diarrhée qu'on aurait passé à travers une chaussette ! (-" + this.energiePerdueQuandMauvaisCafe + " d'énergie)");
            dev.setEnergie(dev.getEnergie() - this.energiePerdueQuandMauvaisCafe);
        }
    }
}
